# LocationTest #

## README ##

LocationTest V1.0

This is an example of using region monitoring in iOS Core Location to trigger more detailed location monitoring.

Once the region is entered, the app logs location updates to both Core Data and Cloud Kit (if available).

The region set up screen specifies the center and radius of the region to trigger updates and how frequently location updates are stored in the database(s)

### Building ###

There are no external dependencies; you can simply pull the repo and open the project in Xcode.  You will need to modify the bundle name and developer identities as usual in order to build the project.

In order to save data to Cloud Kit you will need to define the record type:

1. Access the Cloud Kit dashboard - https://icloud.developer.apple.com/dashboard/

2. Log in with your developer Apple ID

3. Create a new record type called `Footprint` for the `LocationTest` app

4. Add a field of type `Location` named `location` to the `Footprint` record

5. Add a field of type `Date/Time` named `timestamp` to the `Footprint` record

6. Save your changes