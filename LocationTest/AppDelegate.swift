//
//  AppDelegate.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 23/03/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import CloudKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager!
    var lastUpdateTime=NSDate(timeIntervalSince1970: 0)
    var cloudKitAvailable=false
    var cloudData:[CKRecord]?
    var regionStatuses=[String:Bool]()
    var regionRecords=[String:RegionEntry]()
    var currentRegion:RegionEntry?
    var bgTask = UIBackgroundTaskInvalid
    var regions: [Region]!
    var inRegionCount=0
    
    var rate=60 {
        didSet {
            let defaults=NSUserDefaults.standardUserDefaults()
            defaults.setInteger(rate,forKey: "rate")
        }
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes:[ .Alert,.Badge,.Sound], categories: nil))
        
        let br=UIApplication.sharedApplication().backgroundRefreshStatus
        
        if (br == UIBackgroundRefreshStatus.Available) {
            print("BR is available")
        }
        
        let defaults=NSUserDefaults.standardUserDefaults()
        
        self.rate=defaults.integerForKey("rate")
        
        self.loadRegions()
        
        self.checkCloudKit()
        self.setupLocation()
        if let launchOptions = launchOptions {
            
            if (launchOptions[UIApplicationLaunchOptionsLocationKey] != nil) {
                self.showNotification("location launch", text: "relaunched for location services")
                
            }
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NSLog("In background")
        
        for region in self.locationManager.monitoredRegions {
            
            if (self.regionStatuses[region.identifier] == nil) {
                
                if (self.bgTask == UIBackgroundTaskInvalid) {
                    self.bgTask = UIApplication.sharedApplication().beginBackgroundTaskWithName("regionDetermination", expirationHandler: {
                        NSLog("Background task expired")
                        self.bgTask = UIBackgroundTaskInvalid
                    })
                }
                
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue(), {
                    self.locationManager.requestStateForRegion(region)
                })
                
            }
            
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        NSLog("Entering foreground")
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    func checkCloudKit() {
        
        let container=CKContainer.defaultContainer()
        container.accountStatusWithCompletionHandler { (accountStatus:CKAccountStatus, error:NSError?) in
            if (accountStatus == CKAccountStatus.Available) {
                self.cloudKitAvailable=true
                let nc=NSNotificationCenter.defaultCenter()
                nc.postNotificationName("cloudKitAvailable", object: self)
                self.fetchFootPrints()
            }
        }
        
    }
    
    func fetchFootPrints() {
        let container=CKContainer.defaultContainer()
        let database=container.privateCloudDatabase
        let pred=NSPredicate(value: true)
        let sortDescriptor=NSSortDescriptor(key: "timestamp", ascending: true)
        let query=CKQuery(recordType: "Footprint", predicate: pred)
        query.sortDescriptors=[sortDescriptor]
        let queryOp=CKQueryOperation(query: query)
        self.cloudData=[CKRecord]()
        
        queryOp.recordFetchedBlock=self.fetchedAsRecord
        
        
        queryOp.queryCompletionBlock = { (cursor, error) in
            self.retrieveNextRecords(cursor, error:error)
        }
        
        database.addOperation(queryOp)
        database.performQuery(query, inZoneWithID: nil) { (records, error) in
            if (error == nil) {
                self.cloudData=records
                let nc=NSNotificationCenter.defaultCenter()
                nc.postNotificationName("newCloudData", object: self)
            }
        }
    }
    
    private func retrieveNextRecords(cursor:CKQueryCursor?, error:NSError?) {
        if let cursor = cursor {
            print("Got a cursor.")
            // There is more data to fetch
            let moreWork = CKQueryOperation(cursor: cursor)
            moreWork.recordFetchedBlock = self.fetchedAsRecord
            moreWork.queryCompletionBlock =  { (cursor, error) in
                self.retrieveNextRecords(cursor, error:error)
            }
            let container=CKContainer.defaultContainer()
            let database=container.privateCloudDatabase
            database.addOperation(moreWork)
            
        } else {
            print("Done loading")
            let nc=NSNotificationCenter.defaultCenter()
            nc.postNotificationName("newCloudData", object: self)
        }
    }
    
    private func fetchedAsRecord(record:CKRecord!) {
        
        self.cloudData!.append(record)
        print("Cloud count=\(self.cloudData!.count)")
    }
    
    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "me.wilko.LocationTest" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("LocationTest", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as! NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // MARK: - Location & region management
    
    func setupLocation() {
        
        guard CLLocationManager.locationServicesEnabled() else {
            NSLog("Location services unavailable")
            return
        }
        
        self.locationManager=CLLocationManager()
        self.locationManager.delegate=self
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways) {
            self.locationManager.requestAlwaysAuthorization()
        }
        
    }
    
    func addRegion(region:Region) {
        
        for clRegion in self.locationManager.monitoredRegions {
            if (clRegion.identifier == region.name) {
                self.locationManager.stopMonitoringForRegion(clRegion)
                break;
            }
        }
        
        let circularRegion = region.circularRegion
        
        self.locationManager.startMonitoringForRegion(circularRegion)
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue(), {
                self.regionStatuses.removeValueForKey(region.name)
                self.locationManager.requestStateForRegion(circularRegion)
        })

    }
    
    func removeAllRegions()
    {
        for clRegion in self.locationManager.monitoredRegions {
            self.regionStatuses.removeValueForKey(clRegion.identifier)
            self.locationManager.stopMonitoringForRegion(clRegion)
            
        }
        
    }
    
    func removeRegion(region:Region) {
        for clRegion in self.locationManager.monitoredRegions {
            if (clRegion.identifier == region.name) {
                self.locationManager.stopMonitoringForRegion(clRegion)
                break;
            }
        }
        self.regionStatuses.removeValueForKey(region.name)
        self.locationManager.stopUpdatingLocation()
        let nc=NSNotificationCenter.defaultCenter()
        nc.postNotificationName("regionChange", object: self)
    }
    
    func locationAuthOK(manager: CLLocationManager) {
   //     manager.activityType = .Fitness
        
        manager.pausesLocationUpdatesAutomatically=false
        
        if #available(iOS 9.0, *) {
            manager.allowsBackgroundLocationUpdates=true
        }
        
        manager.startMonitoringSignificantLocationChanges()
        
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest
        self.locationManager.distanceFilter=25
        
        self.removeAllRegions()
        
        for region in self.regions {
            self.addRegion(region)
        }
    }
    
    func enterRegion(manager: CLLocationManager, region: CLRegion) {
        NSLog("Entered region: %@",region.identifier)
        
        if (self.regionStatuses[region.identifier] != true) {
            
            self.showNotification("Entered region", text: "Entered region \(region.identifier)")
            self.inRegionCount += 1
            
            if let regionEntry = self.regionRecords[region.identifier] {
                regionEntry.exitTime=NSDate()
            }
            
            let regionEntry=NSEntityDescription.insertNewObjectForEntityForName("RegionEntry", inManagedObjectContext: self.managedObjectContext) as! RegionEntry
            
            regionEntry.entryTime=NSDate()
            if let clRegion = region as? CLCircularRegion {
                regionEntry.latitude=clRegion.center.latitude
                regionEntry.longitude=clRegion.center.longitude
                regionEntry.radius=clRegion.radius
            }
            
            self.regionRecords[region.identifier]=regionEntry
            
            NSLog("Started location updates")
            /*  if #available(iOS 9.0, *) {
             manager.allowsBackgroundLocationUpdates=true
             }*/
            manager.startUpdatingLocation()
            
            self.regionStatuses[region.identifier]=true
            let nc=NSNotificationCenter.defaultCenter()
            nc.postNotificationName("regionChange", object: self)
        }
    }
    
    func exitRegion(manager: CLLocationManager, region:CLRegion) {
        NSLog("Exited region %@",region.identifier)
        
        if (self.regionStatuses[region.identifier] != false) {
            
            if (self.regionStatuses[region.identifier] == true) {
                self.showNotification("Exited region", text: "Exited region \(region.identifier)")
                self.inRegionCount -= 1
                
                if (self.inRegionCount == 0) {
                    NSLog("Stopping location updates")
                    self.locationManager.stopUpdatingLocation()
                }
                
            }
        
            self.regionStatuses[region.identifier]=false
            
            if let currentRegion = self.regionRecords[region.identifier] {
                currentRegion.exitTime=NSDate()
                self.saveContext()
            }
            
            let nc=NSNotificationCenter.defaultCenter()
            nc.postNotificationName("regionChange", object: self)
        }
        
    }
    
    // MARK: - Location manager delegate
    
  /*  func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        self.enterRegion(manager, region: region)
    }
    
    
    
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
       self.exitRegion(manager, region: region)
    }*/
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if  status == CLAuthorizationStatus.AuthorizedAlways {
            self.locationAuthOK(manager)
            
        } else {
            print("Authorisation status=\(status.rawValue)")
        }
    }
   
    func locationManager(manager: CLLocationManager, didDetermineState state: CLRegionState, forRegion region: CLRegion) {
        
        switch state {
        case CLRegionState.Inside:
            NSLog("determined inside")
            self.enterRegion(manager, region: region)
        case CLRegionState.Outside:
            NSLog("determined outside")
            self.exitRegion(manager, region: region)
        case CLRegionState.Unknown:
            NSLog("Region state unknown")
        }
        
        if self.bgTask != UIBackgroundTaskInvalid {
            UIApplication.sharedApplication().endBackgroundTask(self.bgTask)
            self.bgTask=UIBackgroundTaskInvalid
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        NSLog("U")
        for region in self.regions {
            if self.regionStatuses[region.name] == true {
                for location in locations {
                    if location.timestamp.timeIntervalSinceDate(self.lastUpdateTime) >= Double(self.rate) {
                        NSLog("location update: %@",location)
                        self.storeLocation(location,forRegion:region)
                        self.lastUpdateTime=location.timestamp
                    }
                }
            }
            /*if (self.regionState == nil) {
             if let region=self.monitoredRegion {
             self.locationManager.requestStateForRegion(region)
             }
             }*/
        }
    }
    
    func showNotification(title:String,text:String) {
        let notification=UILocalNotification()
        if #available(iOS 8.2, *) {
            notification.alertTitle=title
        } else {
            // Fallback on earlier versions
        }
        notification.alertBody=text
        notification.fireDate=NSDate()
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
        NSLog("Location updates paused")
        self.showNotification("Updates paused", text: "Location updates paused")
    }
    
    func locationManagerDidResumeLocationUpdates(manager: CLLocationManager) {
        NSLog("Location updates resumed")
        self.showNotification("Updates resumed", text: "Location updates resumed")
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        NSLog("Location error:%@",[error.localizedDescription])
    }
    
    //MARK: - Data recording
    
    func storeLocation(location:CLLocation, forRegion region:Region) {
        NSLog("Storing location %@",location)
        
        var task:UIBackgroundTaskIdentifier?
        
        if (self.cloudKitAvailable) {
        
        if UIApplication.sharedApplication().applicationState == .Background {
            task = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler( {
                NSLog("Background task expired")
            }
            )
            
        }
        
        let privateDB = CKContainer.defaultContainer().privateCloudDatabase
        let place = CKRecord(recordType: "Footprint")
        place.setValue(NSDate(), forKey: "timestamp")
        place.setValue(location, forKey: "location")
        privateDB.saveRecord(place) { (place:CKRecord?, error:NSError?) -> Void in
            if let error=error {
                NSLog("Error saving record: %@",error.localizedDescription)
            } else {
                 NSLog("Saved record")
                var cloudData=self.cloudData ?? [CKRecord]()
                cloudData.append(place!)
                self.cloudData=cloudData
                let nc=NSNotificationCenter.defaultCenter()
                nc.postNotificationName("newCloudData", object: self)
               
            }
            if let task=task {
                UIApplication.sharedApplication().endBackgroundTask(task)
            }
        }
        }
        
        let footprint=NSEntityDescription.insertNewObjectForEntityForName("Footprint", inManagedObjectContext: self.managedObjectContext) as! Footprint
        
        footprint.timeStamp=location.timestamp
        footprint.lattitude=location.coordinate.latitude
        footprint.longitude=location.coordinate.longitude
        footprint.regionEntry=self.regionRecords[region.name]!
        
       /* let notification=UILocalNotification()
        if #available(iOS 8.2, *) {
            notification.alertTitle="Saved location update"
        } else {
            // Fallback on earlier versions
        }
        notification.alertBody="Saved location"
        notification.fireDate=NSDate()
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)*/

        
        self.saveContext()
    }
    
    func deleteData() {
        if (self.cloudKitAvailable) {
            self.deleteCloudData()
        }
        
        self.deleteCoreData()
    }
    
    func deleteCloudData() {
        
        let container=CKContainer.defaultContainer()
        
        let privateDatabase=container.privateCloudDatabase
        
        let pred=NSPredicate(value:true)
        
        let query = CKQuery.init(recordType: "Footprint", predicate: pred)
        
        
        privateDatabase.performQuery(query, inZoneWithID: nil) { (records:[CKRecord]?, error:NSError?) in
            let deleteRecords=records?.map( {
                (let record) -> CKRecordID in
                return record.recordID
            })
            
            print("Found \(deleteRecords!.count) things to delete")
            
            let deleteOperation=CKModifyRecordsOperation(recordsToSave: nil, recordIDsToDelete: deleteRecords)
            
            deleteOperation.perRecordProgressBlock={ record, cloudKit_sProgress in
                
                print("Deleted \(record) \(cloudKit_sProgress)")
                
            }//end of block
            
            deleteOperation.completionBlock = {
                print("Deletion completed")
                self.cloudData=nil
                let nc=NSNotificationCenter.defaultCenter()
                nc.postNotificationName("newCloudData", object: self)
            }
            
            privateDatabase.addOperation(deleteOperation)
            
        }
    }
    
    func deleteCoreData() {
        
        let fetchRequest = NSFetchRequest(entityName: "Footprint")
        if #available(iOS 9.0, *) {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            do {
                try self.persistentStoreCoordinator.executeRequest(deleteRequest, withContext: self.managedObjectContext)
            } catch let error as NSError {
                print(error)
            }
        } else {
            
            let allFootprints=NSFetchRequest()
            allFootprints.entity=NSEntityDescription.entityForName("Footprint", inManagedObjectContext: self.managedObjectContext)
            allFootprints.includesPropertyValues=false
        
            do  {
                let footprintObjects=try self.managedObjectContext.executeFetchRequest(allFootprints) as![Footprint]
            
                for footprint in footprintObjects {
                    self.managedObjectContext.deleteObject(footprint)
                }
                
                try self.managedObjectContext.save()
                
            } catch let error as NSError {
                print(error)
            }
            
            
        }
        
    }
    
    func appendRegion(region:Region) -> Void {
        self.regions.append(region)
        self.addRegion(region)
        self.saveRegions()
    }
    
    func deleteRegionAtIndex(index:Int) -> Void {
        
        self.removeRegion(self.regions[index])
        
        self.regions.removeAtIndex(index)
        self.saveRegions()
    }
    
    func updateRegion(region:Region, atIndex index:Int) {
        self.removeRegion(self.regions[index])
        self.regions[index]=region
        self.addRegion(region)
        self.saveRegions()

    }
    
    func saveRegions() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let regionData = NSKeyedArchiver.archivedDataWithRootObject(self.regions)
        defaults.setObject(regionData, forKey: "regions")
    }
    
    func loadRegions() {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let archivedData=defaults.objectForKey("regions") as? NSData {
            self.regions=NSKeyedUnarchiver.unarchiveObjectWithData(archivedData) as! [Region]
        } else {
            self.regions=[Region]()
        }
    }
    
    
    
}

