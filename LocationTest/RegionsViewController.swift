//
//  RegionsViewController.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 20/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class RegionsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, MKMapViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak private var rateSlider: UISlider!
    @IBOutlet weak private var rateLabel: UILabel!
    var frequencies=["continuous","15 seconds","30 seconds","60 seconds"]
    var rate=0
    let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
    var editRegionIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let defaults=NSUserDefaults.standardUserDefaults()
        
        var rateSliderValue=3
        self.rate=30
        
        self.rate=defaults.integerForKey("rate")
        
        switch self.rate {
        case 0:
            rateSliderValue=0
        case 15:
            rateSliderValue=1
        case 30:
            rateSliderValue=2
        default:
            rateSliderValue=3
        }
        
        self.rateSlider.value=Float(rateSliderValue)
        
        self.rateLabel.text=self.frequencies[rateSliderValue]
        
        self.mapView.showsUserLocation=true
        self.mapView.userTrackingMode = .Follow
        
        self.editButton.possibleTitles=NSSet(array: ["Edit","End Editing"]) as? Set<String>
        
        self.setOverlays()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.appDelegate.rate=self.rate
    }
    
    @IBAction func sliderChanged(sender:UISlider) {
        
        let value=Int(sender.value)
        
        switch value {
        case 0:
            self.rate=0
        case 1:
            self.rate=15
        case 2:
            self.rate=30
        case 3:
            self.rate=60
        default:
            self.rate=60
        }
        
        self.rateLabel.text=self.frequencies[value];
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       let count = self.appDelegate.regions.count+1
        
       self.editButton.enabled = (count > 1)
        
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("regionCell", forIndexPath: indexPath) as! RegionCell
        
        if indexPath.row == self.appDelegate.regions.count {
            cell.regionLabel.text="Add.."
        } else {
            cell.regionLabel.text=appDelegate.regions[indexPath.row].name
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (tableView.editing || indexPath.row == self.appDelegate.regions.count) {
            self.performSegueWithIdentifier("regionSegue", sender: indexPath)
        } else {
            let region = self.appDelegate.regions[indexPath.row]
            self.mapView.setCenterCoordinate(region.location, animated: true)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "regionSegue") {
            let destVC = segue.destinationViewController as! RegionViewController
            let indexPath = sender as! NSIndexPath
            self.editRegionIndex=indexPath.row
            if (indexPath.row < self.appDelegate.regions.count) {
                destVC.region = self.appDelegate.regions[indexPath.row]
            } else {
                destVC.region = Region(name:"Region \(indexPath.row)", location: self.mapView.userLocation.coordinate, radius: 100)
            }
        }
        
    }
    
    @IBAction func unwind(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func unwindAndSave(segue:UIStoryboardSegue) {
        guard let regionIndex = self.editRegionIndex else {
            return
        }
        
        let regionVC = segue.sourceViewController as! RegionViewController
        let indexPath = NSIndexPath(forRow: self.editRegionIndex!, inSection: 0)
        if (regionIndex < self.appDelegate.regions.count) {
            self.appDelegate.updateRegion(regionVC.region!,atIndex:self.editRegionIndex!)
            self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        } else {
            self.appDelegate.appendRegion(regionVC.region!)
            self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
        
        self.setOverlays()
        
    }

    @IBAction func editTapped(sender: AnyObject) {

        self.tableView.setEditing(!self.tableView.editing, animated: true)
        if (self.tableView.editing) {
            self.editButton.title="End editing"
        } else {
            self.editButton.title="Edit"
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
         return !(indexPath.row == self.appDelegate.regions.count)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch (editingStyle) {
        
        case .Delete:
            self.appDelegate.deleteRegionAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            if (self.appDelegate.regions.count == 0) {
                tableView.setEditing(false, animated: true)
                self.editButton.title="Edit"
            }
            self.setOverlays()
       
        case .Insert:
            break;
            
        case .None:
            break
        }
    }
    
    func setOverlays() {
        
        self.mapView.removeOverlays(self.mapView.overlays)
        
        for region in self.appDelegate.regions {
            
            let circularRegion=CLCircularRegion(center: region.location, radius: region.radius, identifier: region.name)
                
            // Add an overlay
            let circle = MKCircle(centerCoordinate: circularRegion.center, radius: circularRegion.radius)
            self.mapView.addOverlay(circle)
        }
        
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = UIColor.blueColor()
        circle.fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
        circle.lineWidth = 1
        return circle
        
    }
}