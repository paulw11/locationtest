//
//  RegionEntry+CoreDataProperties.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 3/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension RegionEntry {

    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var entryTime: NSDate?
    @NSManaged var exitTime: NSDate?
    @NSManaged var radius: NSNumber?
    @NSManaged var footprints: NSOrderedSet?

}
