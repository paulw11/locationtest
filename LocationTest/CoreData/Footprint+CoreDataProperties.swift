//
//  Footprint+CoreDataProperties.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 3/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Footprint {

    @NSManaged var lattitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var timeStamp: NSDate?
    @NSManaged var regionEntry: RegionEntry?

}
