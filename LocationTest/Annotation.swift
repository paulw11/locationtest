//
//  Annotation.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 23/03/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject,MKAnnotation {

        var coordinate: CLLocationCoordinate2D
        var title: String?
        
        init(coordinate: CLLocationCoordinate2D, title: String) {
            self.coordinate = coordinate
            self.title = title
        }
}
