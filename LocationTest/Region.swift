//
//  Region.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 20/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import Foundation
import CoreLocation

class Region : NSObject,NSCoding {
    
    var name : String
    var location:CLLocationCoordinate2D
    var radius:CLLocationDistance
    
    var circularRegion: CLCircularRegion {
        return CLCircularRegion(center:self.location, radius:self.radius, identifier:self.name)
    }
    
    init(name:String, location: CLLocationCoordinate2D, radius: CLLocationDistance) {
        
        self.name=name
        self.location=location
        self.radius=radius
        
        super.init()
    }
    
    required init(coder:NSCoder) {
        
        self.name = coder.decodeObjectForKey("name") as! String
        let lat = coder.decodeDoubleForKey("lat")
        let lon = coder.decodeDoubleForKey("lon")
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        self.location=location
        self.radius = coder.decodeDoubleForKey("radius")
        
        super.init()
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.name, forKey: "name")
        aCoder.encodeDouble(self.location.latitude, forKey: "lat")
        aCoder.encodeDouble(self.location.longitude, forKey: "lon")
        aCoder.encodeDouble(self.radius, forKey: "radius")
    }
    
    
}