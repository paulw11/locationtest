//
//  RegionViewController.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 24/03/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class RegionViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak private var mapView: MKMapView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak private var lattitudeTF: UITextField!
    @IBOutlet weak private var radiusTF: UITextField!
    @IBOutlet weak private var longitudeTF: UITextField!
    
    var region: Region?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let nc=NSNotificationCenter.defaultCenter()
        
        nc.addObserver(self, selector: #selector(RegionViewController.textFieldValueChanged), name: UITextFieldTextDidChangeNotification, object: self.radiusTF)
        nc.addObserver(self, selector: #selector(RegionViewController.textFieldValueChanged), name: UITextFieldTextDidChangeNotification, object: self.lattitudeTF)
        nc.addObserver(self, selector: #selector(RegionViewController.textFieldValueChanged), name: UITextFieldTextDidChangeNotification, object: self.longitudeTF)
        nc.addObserver(self, selector: #selector(RegionViewController.textFieldValueChanged), name: UITextFieldTextDidChangeNotification, object: self.nameTF)
        
        
        let gesture=UILongPressGestureRecognizer(target: self, action: #selector(RegionViewController.longpress(_:)))
        
        self.mapView.addGestureRecognizer(gesture)
        
    }
    
    func longpress(gestureRecongizer:UIGestureRecognizer) {
        
        guard gestureRecongizer.state == UIGestureRecognizerState.Began else {
            return
        }
        
        let touchPoint=gestureRecongizer.locationInView(self.mapView)
        let coordinate=self.mapView.convertPoint(touchPoint, toCoordinateFromView: self.mapView)
        
        self.lattitudeTF.text="\(coordinate.latitude)"
        self.longitudeTF.text="\(coordinate.longitude)"
        
        self.mapView.setCenterCoordinate(coordinate, animated: true)
        self.newRegion()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        guard self.region != nil else {
            return
        }
        
        self.lattitudeTF.text="\(self.region!.location.latitude)"
        self.longitudeTF.text="\(self.region!.location.longitude)"
        self.radiusTF.text="\(self.region!.radius)"
        self.nameTF.text=self.region!.name
        
        self.setMapViewToLocation(self.region!.location)
        
        self.newRegion()
    
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        let nc=NSNotificationCenter.defaultCenter()
        
        nc.removeObserver(self)
    }
    
    func setMapViewToLocation(location:CLLocationCoordinate2D) {
        
        let span=MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        
        let region=MKCoordinateRegion(center: location, span: span)
        
        self.mapView.setRegion(region, animated: false)
        
       
        
    }
    
    func newRegion() {
        
        self.mapView.removeOverlays(self.mapView.overlays)
        
        self.region=self.createRegion()
        
        if self.region != nil {
            
            let circularRegion=CLCircularRegion(center: self.region!.location, radius: self.region!.radius, identifier: self.region!.name)
            
            // Add an overlay
            let circle = MKCircle(centerCoordinate: circularRegion.center, radius: circularRegion.radius)
            self.mapView.addOverlay(circle)
        }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = UIColor.blueColor()
        circle.fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
        circle.lineWidth = 1
        return circle
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancelTapped(sender: AnyObject) {
        self.performSegueWithIdentifier("unwindSegue", sender: self)
    }
    
    
    
  /*  @IBAction func OKTapped(sender: AnyObject) {
        
        if let region=self.createRegion() {
            
            let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
            
            appDelegate.monitoredRegion=region
            
            self.performSegueWithIdentifier("unwindSegue", sender: self)
        }
    }*/
    
    
    func createRegion() -> Region? {
        
        let name = self.nameTF.text!
        let latitude=Double(self.lattitudeTF.text!)
        let longitude=Double(self.longitudeTF.text!)
        let radius=Double(self.radiusTF.text!)
        
        guard latitude != nil && longitude != nil && radius != nil else {
            return nil
        }
        
        let coordinate=CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
        
        let region=Region(name:name, location:coordinate, radius: radius!)
        
        return region
    }
        
    @IBAction func locationTapped(sender: AnyObject) {
        
        let userLocation=self.mapView.userLocation.coordinate
        
        self.lattitudeTF.text="\(userLocation.latitude)"
        self.longitudeTF.text="\(userLocation.longitude)"
        
        self.setMapViewToLocation(self.mapView.userLocation.coordinate)

        self.newRegion()
        
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldValueChanged() {
         self.newRegion()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentString=textField.text! as NSString
        
        let newString=currentString.stringByReplacingCharactersInRange(range, withString: string)
        
        if Double(newString) != nil || newString.isEmpty {
            return true
        } else {
            return false
        }
        
    }
    
    

}
