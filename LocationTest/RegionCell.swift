//
//  RegionCell.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 20/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import Foundation
import UIKit

class RegionCell: UITableViewCell {
    
    @IBOutlet weak var regionLabel: UILabel!
}