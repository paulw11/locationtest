//
//  ViewController.swift
//  LocationTest
//
//  Created by Paul Wilkinson on 23/03/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import CloudKit
import MessageUI


class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate,MKMapViewDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var followItem: UIBarButtonItem!
    @IBOutlet weak var deleteItem: UIBarButtonItem!
    @IBOutlet weak var mailItem: UIBarButtonItem!
    @IBOutlet weak var segementedControl: UISegmentedControl!
    @IBOutlet weak var regionStateLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var managedObjectContext:NSManagedObjectContext!
    
    var appDelegate:AppDelegate!
    
    var cloudData:[CKRecord]?
    
    var dateFormatter : NSDateFormatter!
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest(entityName: "Footprint")
        
        // Add Sort Descriptors
        let sectionSortDescriptor = NSSortDescriptor(key:"regionEntry.entryTime", ascending: true)
        let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: true)
        fetchRequest.sortDescriptors = [sectionSortDescriptor,sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: "regionEntry.entryTime", cacheName: nil)
       // let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    @IBAction func mailData(sender: AnyObject) {
        
        let root = GPXRoot(creator:"Location Test")
        
        for section in 0..<self.fetchedResultsController.sections!.count {
            
            let track=root.newTrack()
            
            track.name=sectionHeaderForSection(section)
            
            let sectionInfo = self.fetchedResultsController.sections![section]
            for row in 0..<sectionInfo.numberOfObjects {
                
                let indexPath=NSIndexPath(forRow: row, inSection: section)
                let footprint=self.fetchedResultsController.objectAtIndexPath(indexPath) as! Footprint
                let lat=CGFloat(footprint.lattitude!.floatValue)
                let lon=CGFloat(footprint.longitude!.floatValue)
                let trackPoint=track.newTrackpointWithLatitude(lat,longitude:lon)
                trackPoint.time=footprint.timeStamp!
                
            }
        }
        
        let gpx=root.gpx()
        let composer=MFMailComposeViewController()
        composer.mailComposeDelegate=self
        composer.setSubject("Your GPX File")
        composer.addAttachmentData(gpx.dataUsingEncoding(NSUTF8StringEncoding)!,mimeType:"application/gpx",fileName:"track.gpx")
        self.presentViewController(composer,animated:true, completion:nil)
        
    }


    @IBAction func deleteData(sender: AnyObject) {
     
        
        let alertController=UIAlertController(title: "Delete data", message: "Remove all tracking data from local and server storage?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        alertController.popoverPresentationController?.barButtonItem=self.deleteItem
        
        let cancelAction=UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        let deleteAction = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive) { (alertAction:UIAlertAction) in
            let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.deleteData()
            do {
                try self.fetchedResultsController.performFetch()
                self.tableView.reloadData()
            } catch {
                let fetchError = error as NSError
                print("\(fetchError), \(fetchError.userInfo)")
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        self.dateFormatter=NSDateFormatter()
        self.dateFormatter.dateStyle = .ShortStyle
        self.dateFormatter.timeStyle = .ShortStyle
        
        self.appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedObjectContext=self.appDelegate.managedObjectContext
        
        let nc=NSNotificationCenter.defaultCenter()
        
        nc.addObserver(self, selector: #selector(ViewController.newRegion), name: "newRegion", object: nil)
        nc.addObserver(self, selector: #selector(ViewController.cloudKitAvailable), name:"cloudKitAvailable", object:nil)
        nc.addObserver(self, selector: #selector(ViewController.newCloudData), name:"newCloudData", object:nil)
        nc.addObserver(self, selector: #selector(ViewController.regionChange), name:"regionChange", object:nil)
        
        if !MFMailComposeViewController.canSendMail() {
            self.mailItem.enabled=false
        }
        
        
        do {
            try self.fetchedResultsController.performFetch()
            self.tableView.reloadData()
        } catch {
            let fetchError = error as NSError
            print("\(fetchError), \(fetchError.userInfo)")
        }
        
        self.regionChange()
       
    }
    
    func regionChange() {
        
        var state: Bool?
        
        for region in self.appDelegate.regions {
            if let status=self.appDelegate.regionStatuses[region.name] {
                if status == true {
                    state = true
                    break;
                } else {
                    state = false
                }
            }
        }
        
        if let state=state {
            if (state) {
                self.regionStateLabel.text="Inside region"
                self.regionStateLabel.backgroundColor=UIColor.init(colorLiteralRed:0.000, green:0.502, blue:0.000, alpha:0.500)
                self.regionStateLabel.textColor=UIColor.whiteColor()
            } else {
                self.regionStateLabel.text="Outside region"
                self.regionStateLabel.backgroundColor=UIColor.init(colorLiteralRed: 0.502, green: 0.00, blue: 0.000, alpha: 0.8)
                self.regionStateLabel.textColor=UIColor.whiteColor()
                self.tableView.reloadData()
            }
        } else {
            self.regionStateLabel.text="Region state unknown"
            self.regionStateLabel.backgroundColor=UIColor.init(colorLiteralRed:1.000, green:1.00, blue:0.400, alpha:0.500)
            self.regionStateLabel.textColor=UIColor.blackColor()
        }
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        self.mapView.showsUserLocation=true
        self.mapView.userTrackingMode = .Follow
        self.followItem.image=UIImage(named:"MapFilled")
        
        let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
        
        self.mapView.removeOverlays(self.mapView.overlays)
        
        self.segementedControl.setEnabled(appDelegate.cloudKitAvailable, forSegmentAtIndex: 1)
        
        if (appDelegate.regions.count != 0 ) {
            
            self.newRegion()
        }
    }
    
    
    func newRegion() {
        let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
        
        self.mapView.removeOverlays(self.mapView.overlays)
        
        for region in appDelegate.regions {
            
            let circularRegion=CLCircularRegion(center: region.location, radius: region.radius, identifier: region.name)
            
            // Add an overlay
            let circle = MKCircle(centerCoordinate: circularRegion.center, radius: circularRegion.radius)
            self.mapView.addOverlay(circle)
        }
    }
    
    func cloudKitAvailable() {
        self.segementedControl.setEnabled(true, forSegmentAtIndex: 1)
    }
    
    func newCloudData() {
        dispatch_async(dispatch_get_main_queue(), {
            let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
            if (self.segementedControl.selectedSegmentIndex==1) {
                if (self.cloudData == nil || appDelegate.cloudData == nil) {
                    self.cloudData=appDelegate.cloudData
                    self.tableView.reloadData()
                } else if (appDelegate.cloudData!.count < self.cloudData!.count) {
                    self.cloudData=appDelegate.cloudData
                    self.tableView.reloadData()
                } else {
                    var newRows=[NSIndexPath]()
                    for i in self.cloudData!.count ..< appDelegate.cloudData!.count {
                        self.cloudData!.append(appDelegate.cloudData![i])
                        newRows.append(NSIndexPath(forRow: i, inSection: 0))
                    }
                    self.tableView.insertRowsAtIndexPaths(newRows, withRowAnimation: UITableViewRowAnimation.Automatic)
                }
            } else {
                self.cloudData=appDelegate.cloudData
            }
        })
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.greenColor()
            circle.fillColor = UIColor(red: 0, green: 2555, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.segementedControl.selectedSegmentIndex==0 {
            if let sections=self.fetchedResultsController.sections {
                return sections.count
                
            }
        } else {
            if let _ = self.cloudData {
                return 1
            }
        }
        return 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segementedControl.selectedSegmentIndex==0 {
            if let sections = fetchedResultsController.sections {
                let sectionInfo = sections[section]
                return sectionInfo.numberOfObjects
            }
        } else {
            if let cloudData=self.cloudData {
                print("\(cloudData.count) cloud rows")
                return cloudData.count
            }
        }
        
        return 0
    }
        
        func sectionHeaderForSection(section: Int) -> String? {
            guard self.fetchedResultsController.sections != nil && self.segementedControl.selectedSegmentIndex == 0  else {
                return nil
            }
            
            let indexPath=NSIndexPath(forRow: 0, inSection: section)
            let footprint=self.fetchedResultsController.objectAtIndexPath(indexPath) as! Footprint
            
            let regionEntry = footprint.regionEntry!
            
            let entryString=self.dateFormatter.stringFromDate(regionEntry.entryTime!)
            
            var exitString : String?
            
            if (regionEntry.exitTime != nil ) {
                exitString=self.dateFormatter.stringFromDate(regionEntry.exitTime!)
            }
            
            if (exitString != nil) {
                return "\(entryString) - \(exitString!)"
            } else {
                return "\(entryString)"
            }
        }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return self.sectionHeaderForSection(section)
    
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        
        if (self.segementedControl.selectedSegmentIndex==0) {
        
        let footprint=self.fetchedResultsController.objectAtIndexPath(indexPath) as! Footprint
        
        cell.textLabel!.text=String.init(format: "%0.3f %0.3f", arguments: [footprint.lattitude!.doubleValue,footprint.longitude!.doubleValue])
        cell.detailTextLabel!.text=NSDateFormatter.localizedStringFromDate(footprint.timeStamp!, dateStyle: NSDateFormatterStyle.MediumStyle, timeStyle: NSDateFormatterStyle.MediumStyle)
        } else {
            let footprint=self.cloudData![indexPath.row]
            let location=footprint.objectForKey("location") as! CLLocation
            let timestamp=footprint.objectForKey("timestamp") as! NSDate
            cell.textLabel!.text=String.init(format: "%0.3f %0.3f", arguments: [location.coordinate.latitude,location.coordinate.longitude])
            cell.detailTextLabel!.text=NSDateFormatter.localizedStringFromDate(timestamp, dateStyle: NSDateFormatterStyle.MediumStyle, timeStyle: NSDateFormatterStyle.MediumStyle)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let footprint = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Footprint
        
        let coordinate = CLLocationCoordinate2D(latitude: footprint.lattitude!.doubleValue, longitude: footprint.longitude!.doubleValue)
        
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        self.mapView.addAnnotation(Annotation(coordinate: coordinate,title: footprint.timeStamp!.description))
        self.zoomToLocation(coordinate)
        
    }
    
    // MARK: -
    // MARK: Fetched Results Controller Delegate Methods
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        if (self.segementedControl.selectedSegmentIndex==0) {
        tableView.beginUpdates()
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        if (self.segementedControl.selectedSegmentIndex==0) {
        tableView.endUpdates()
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        if (self.segementedControl.selectedSegmentIndex == 0) {
            switch (type) {
            case .Insert:
                print("Insert \(self.fetchedResultsController.sections![0].numberOfObjects)")
                if let indexPath = newIndexPath {
                    tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                }
                break;
            case .Delete:
                if let indexPath = indexPath {
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                }
                break;
            case .Update:
                if let indexPath = indexPath {
                    self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                }
                break;
            case .Move:
                if let indexPath = indexPath {
                    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                }
                
                if let newIndexPath = newIndexPath {
                    tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Automatic)
                }
                break;
            }
        }
    }
    
    func controller(controller: NSFetchedResultsController,
                               didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
                                                atIndex sectionIndex: Int,
                                                        forChangeType type: NSFetchedResultsChangeType) {
        if (self.segementedControl.selectedSegmentIndex == 0) {
            switch (type) {
            case .Insert:
                let indexSet=NSIndexSet(index: sectionIndex)
                self.tableView.insertSections(indexSet, withRowAnimation: .Automatic)
                if (sectionIndex > 0) {
                    let oldIndexSet=NSIndexSet(index: sectionIndex-1)
                    self.tableView.reloadSections(oldIndexSet, withRowAnimation: .None)
                }
            case .Delete:
                let indexSet=NSIndexSet(index: sectionIndex)
                self.tableView.deleteSections(indexSet, withRowAnimation: .Automatic)
            default:
                print("Unknown change type for section")
            }
        }
        
    }
    
    //MARK: -
    //MARK: MKMapViewDelegate
    
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
   /*     if (mapView.userTrackingMode != .None ) {
            self.zoomToLocation(userLocation.coordinate)
            mapView.userTrackingMode = .None
        }*/
        
    }
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
         if (self.mapView.userTrackingMode == .None) {
             self.followItem.image=UIImage(named:"Map")
        }
    }

    func zoomToLocation(location:CLLocationCoordinate2D) {
        let span=MKCoordinateSpan(latitudeDelta: 0.05,longitudeDelta: 0.05)
        let region=MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func followItemTapped(sender: AnyObject) {
        
        var itemImage=UIImage(named: "MapFilled")
        var trackingMode = MKUserTrackingMode.Follow
        
        if (self.mapView.userTrackingMode != .None) {
            trackingMode = .None
            itemImage=UIImage(named:"Map")
        }
        
        self.followItem.image=itemImage
        self.mapView.userTrackingMode=trackingMode
    }
    
    @IBAction func segmentSelected(sender:UISegmentedControl) {
        if sender.selectedSegmentIndex==1 {
            let appDelegate=UIApplication.sharedApplication().delegate as! AppDelegate
            self.cloudData=appDelegate.cloudData
        }
        self.tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "regionsSegue" {
         /*   let destVC=segue.destinationViewController as! RegionViewController
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            if (appDelegate.monitoredRegion != nil) {
                destVC.location=appDelegate.monitoredRegion!.center
                destVC.radius=appDelegate.monitoredRegion!.radius
            } else {
                destVC.location=self.mapView.userLocation.coordinate
                destVC.radius=100.0
            }*/
            
        }
    }
    
    @IBAction func unwind(segue:UIStoryboardSegue) {
        
    }
    
    // MARK: - 
    // MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(controller: MFMailComposeViewController,
    didFinishWithResult result: MFMailComposeResult, error: NSError?) {
    // Check the result or perform other tasks.
    
    // Dismiss the mail compose view controller.
    controller.dismissViewControllerAnimated(true, completion: nil)
    }
    

}

